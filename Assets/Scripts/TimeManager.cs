﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public static TimeManager instance;
    [SerializeField] private float slowdownFactor = 0.05f;
    [SerializeField] private float slowdownLength = 2f;
    private void Awake()
    {
        instance = this;
    }
    private void Update()
    {
        
        
    }
    public void DoSlowmotion()
    {
        //Slow timescale like in super hot
        Time.timeScale = slowdownFactor;
        Time.fixedDeltaTime = Time.timeScale * .02f;
    }
    public void BackToNormal()
    {
        // make back to timescale normal
        if (Time.timeScale != 1)
        {
            Time.timeScale += (1f / slowdownLength) * Time.unscaledDeltaTime;
            Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
        }

    }
}
