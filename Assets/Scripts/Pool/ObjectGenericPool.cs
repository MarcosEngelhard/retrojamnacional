﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGenericPool<T> : MonoBehaviour where T : Component
{
    [SerializeField] private T prefab;
    public static ObjectGenericPool<T> intstance { get; private set; }
    // FIFO - First in First out
    private Queue<T> objects = new Queue<T>();
    // Start is called before the first frame update
    void Start()
    {
        intstance = this;
    }
    public T Get()
    {
        if (objects.Count == 0)
        {
            // If Queu is empty addObject
            AddObject();
        }
        return objects.Dequeue();
    }
    public void ReturnToPool(T objectToReturn)
    { // return component to the Queue
        objectToReturn.gameObject.SetActive(false);
        objects.Enqueue(objectToReturn);
    }

    private void AddObject()
    { // instantiate another prefab
        var newObject = Instantiate(prefab);
        newObject.gameObject.SetActive(false);
        objects.Enqueue(newObject);
    }
}
