﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disfunctional : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] private float jumpHeight = 15f;
    [SerializeField]private int maxRandom = 20;
    private int rng;
    private int makeFunctionaltries;
    [SerializeField] private int maxTries = 12;
    [SerializeField]private int maxTriesToBeNormal = 10;
    private int triesToBeNormal;
    private int manyJumpCounts = 0;
    private enum State
    {
        Normal, Disfunctional
    }
    [SerializeField] private State state;
    // Start is called before the first frame update
    void Start()
    {

        rb = GetComponent<Rigidbody2D>();
        triesToBeNormal = 9;
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.Normal:
                manyJumpCounts++;
                NormalJump();
                if(manyJumpCounts >= triesToBeNormal)
                {
                    state = State.Disfunctional;
                }
                break;
            case State.Disfunctional:
                SpamToJump();
                break;
        }
    }
    private void NormalJump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
        
    }
    private void SpamToJump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rng = Random.Range(0, maxRandom + 1);
            Debug.Log(rng);
            if (rng == 0)
            {
                Jump();
                state = State.Normal;
            }
        }
    }
    private void Jump()
    {
        // Jump
        rb.AddForce(Vector2.up * jumpHeight, ForceMode2D.Impulse);
    }
}
