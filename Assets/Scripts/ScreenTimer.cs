﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class ScreenTimer : MonoBehaviour
{
    public static ScreenTimer instance;
    
    [SerializeField]
    private Text timerText;
    private TimeSpan timePlaying;
    private bool timerRunning;
    private float elapsedTime;
    private PlayerHealth player;

    private void Awake()
    {
        instance = this;
        player = FindObjectOfType<PlayerHealth>();
    }

    private void Start()
    {
        timerText.text = "00:00.00";
        if(player.Lifes == 3)
        {
            timerText.gameObject.SetActive(false);
        }
        else
        {
            BeginTimer();
        }
        
        //timerRunning = false;
    }

    public void BeginTimer()
    {
        timerText.gameObject.SetActive(true);
        timerRunning = true;
        elapsedTime = 0f;

        StartCoroutine(UpdateTimer());
    }
    
    public void EndTimer()
    {
        timerRunning = false;
    }

    private IEnumerator UpdateTimer()
    {
        while (timerRunning)
        {
            
            elapsedTime += Time.deltaTime;
            timePlaying = TimeSpan.FromSeconds(elapsedTime);
            string timePlayingStr = timePlaying.ToString("mm':'ss'.'ff");
            timerText.text = timePlayingStr;

            yield return null;
        }
    }


}
