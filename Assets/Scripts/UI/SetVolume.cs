﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SetVolume : MonoBehaviour
{
    public AudioMixer mixer;
    public Slider slider;
    public Toggle toggle;
    // Start is called before the first frame update
    void Start()
    {
        slider.value = PlayerPrefs.GetFloat("MusicVolume", 0.75f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetLevel(float sliderVolume)
    {
        mixer.SetFloat("MusicVol", Mathf.Log10(sliderVolume) * 20);
        PlayerPrefs.SetFloat("MusicVolume", sliderVolume);
    }
    public void FullScreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
        Debug.Log("Fullscreen is" + isFullscreen);
    }
}
