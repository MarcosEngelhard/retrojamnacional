﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helicopter : MonoBehaviour
{
    private BoxCollider2D bc;
    [SerializeField]private Sprite helicopterExplosion;
    private SpriteRenderer spriteRenderer;
    // Start is called before the first frame update
    void Start()
    {
        bc = GetComponent<BoxCollider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent<RocketExplosion>(out RocketExplosion explosion))
        {
            bc.enabled = false;
            spriteRenderer.sprite = helicopterExplosion; // Swap sprite for the explosion
            StartCoroutine(ChopperDestroyedCutscene());
            
        }
        if(collision.TryGetComponent<PlayerHealth>(out PlayerHealth player))
        {
            GameManager.gminstance.DisplayGetChopperImage();
        }
    }
    private IEnumerator ChopperDestroyedCutscene()
    {
        yield return new WaitForSeconds(1.5f);
        GameManager.gminstance.DisplayDestroyChopperImage();
    }
}
