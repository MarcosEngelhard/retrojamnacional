﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour
{
    private Rigidbody2D rb;
    private BoxCollider2D bc;
    [SerializeField] private float speed = 15f;
    private Enemy enemy;
    [SerializeField] private float timeDurability = 10f;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        bc = GetComponent<BoxCollider2D>();
        enemy = FindObjectOfType<Enemy>();
    }
    private void OnEnable()
    {
        StartCoroutine(ToDissappear());
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = transform.right * speed;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Return();
        }
    }
    private IEnumerator ToDissappear()
    {
        yield return new WaitForSeconds(timeDurability);
        Return();
    }
    private void Return()
    {
        //Make spike return to pool;
        transform.parent = enemy.transform;
        enemy.spikePool.ReturnToPool(this);
    }
}
