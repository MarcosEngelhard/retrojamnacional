﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonsBehaviour : MonoBehaviour
{
    [SerializeField]private Image mainMenuBackground;
    [SerializeField]private Image creditsBackground;
    [SerializeField] private Image optionBackground;
    private PlayerHealth player;
    // Start is called before the first frame update
    void Start()
    {
        if(mainMenuBackground != null)
        {
            mainMenuBackground.gameObject.SetActive(true);
            creditsBackground.gameObject.SetActive(false);
            optionBackground.gameObject.SetActive(false);
        }
        player = FindObjectOfType<PlayerHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StartGame()
    {// Start Game
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void ReturnMainMenu()
    {
        if(player != null)
        {
            player.Lifes = 3;
        }
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    public void QuitGame()
    { // Quit Game
        Application.Quit();
    }
    public void Credits()
    { // Display Credits
        mainMenuBackground.gameObject.SetActive(false);
        creditsBackground.gameObject.SetActive(true);
    }
    public void Options()
    {
        //Display Options Menu
        mainMenuBackground.gameObject.SetActive(false);
        optionBackground.gameObject.SetActive(true);
    }
    public void Back()
    { // display back the main menu background
        mainMenuBackground.gameObject.SetActive(true);
        creditsBackground.gameObject.SetActive(false);
        optionBackground.gameObject.SetActive(false);
    }
}
