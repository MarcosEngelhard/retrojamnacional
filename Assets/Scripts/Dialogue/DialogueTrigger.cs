﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    [SerializeField] private Dialogue dialogue;
    [SerializeField]private DialogueManager dialogueManager;
    private PlayerHealth player;
    // Start is called before the first frame update
    void Awake()
    {
        player = FindObjectOfType<PlayerHealth>();
        dialogueManager = FindObjectOfType<DialogueManager>();
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void TriggerDialogue()
    {
        if(player.Lifes == 3)
        {
            
            dialogueManager.StartDialogue(dialogue);
        }
        else
        {
            
            dialogueManager.EndDialogue();
        }
        
    }
}
