﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    private Queue<string> sentences; // FIFO - First in, First out
    private Cutscene cutscene;
    [SerializeField] private Text dialogueText;
    public static AudioSource audioSource;
    [SerializeField] private AudioClip textClip;
    private string sentence;
    private void Awake()
    {
        sentences = new Queue<string>();
        audioSource = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
        cutscene = FindObjectOfType<Cutscene>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Q))
        {
            EndDialogue();
        }
    }
    public void StartDialogue(Dialogue dialogue)
    {
        sentences.Clear(); // Clear whatver you have in the queu

        foreach(string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        DisplayNextSentence();
    }
    public void DisplayNextSentence()
    {
        if(sentences.Count == 0)
        { // When there's no more sentences in Queue
            EndDialogue();
            return;
        }
        // In case we already have sentences in queue
        sentence = sentences.Dequeue();
        
        StartCoroutine(TypeSentence(sentence));
    }
    private IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        
        foreach(char letter in sentence.ToCharArray())
        {
            char.ToUpper(letter); // make the letter bigger since our font has only bigger letters
            if (letter != ' ')
            {               
                //if (!audioSource.isPlaying)
                //{
                    audioSource.PlayOneShot(textClip);
                //}              
            }
            else
            {
                audioSource.Stop();
            }
            
            dialogueText.text += letter;
            yield return new WaitForSeconds(0.1f);
        }

        Invoke("DisplayNextSentence", 5f);
    }
    public void EndDialogue()
    {
        StopAllCoroutines();
        sentences.Clear();
        cutscene.ToGame();
        GameManager.gminstance.ChangeClipForMainAudioSource();
        StopCoroutine(TypeSentence(sentence));
    }
    
}
