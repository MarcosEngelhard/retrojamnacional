﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class PlayerHealth : MonoBehaviour
{
    public static int lifes = 3; 
    private Enemy enemy;
    [SerializeField] private int highscore;
    [SerializeField]private Image loseLifeScreen;
    [SerializeField]private Image gameOverScreen;
    [SerializeField]private Text lifesLeftText;
    [SerializeField] private float maxDistance = 20f;
    private SpriteRenderer spriteRenderer;
    private BoxCollider2D bc;
    private Rigidbody2D rb;
    private bool isDead = false;
    [SerializeField]private GameObject leftWall;
    [SerializeField]private GameObject RightWall;
    private bool canTeleport = true;
    [SerializeField] private float tpCouldownDuration = 1f;
    private Camera cameraMain;
    private AudioSource audioSource;
    [SerializeField] private AudioClip gameOver; 
    public int Lifes
    {
        get
        {
            return lifes;
        }
        set
        {
            lifes = value;
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        enemy = FindObjectOfType<Enemy>();
        loseLifeScreen.gameObject.SetActive(false);
        gameOverScreen.gameObject.SetActive(false);
        spriteRenderer = GetComponent<SpriteRenderer>();
        bc = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        cameraMain = Camera.main;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if(Vector2.Distance(transform.position, enemy.transform.position) > maxDistance
            && transform.position.y < enemy.transform.position.y)
        {
            if (isDead)
                return;
            Debug.Log("More, caralho!");
            LoseLife();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent<Enemy>(out Enemy enemy))
        {
            // Trigger with Enemy
            LoseLife();

        }
        if(collision.TryGetComponent<Spike>(out Spike spike))
        {
            LoseLife();
        }
        if(collision.TryGetComponent<FireBall>(out FireBall fireBall))
        {
            LoseLife();
        }
        if (collision.gameObject.CompareTag("Laser"))
        {
            LoseLife();
        }
        if(collision.gameObject == leftWall)
        {
            if (canTeleport) 
            { // teleport in case it can tp
                transform.position = new Vector3(RightWall.transform.position.x, transform.position.y, transform.position.z);
                cameraMain.transform.position = new Vector3(transform.position.x, cameraMain.transform.position.y, cameraMain.transform.position.z);
                StartCoroutine(CouldownTP());
            }
            
        }
        if(collision.gameObject == RightWall)
        {
            if (canTeleport)
            {
                transform.position = new Vector3(leftWall.transform.position.x, transform.position.y, transform.position.z);
                cameraMain.transform.position = new Vector3(transform.position.x, cameraMain.transform.position.y, cameraMain.transform.position.z);
                StartCoroutine(CouldownTP());
            }
            
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject == leftWall)
        {
            if (canTeleport)
            { // teleport in case it can tp
                transform.position = new Vector3(RightWall.transform.position.x, transform.position.y, transform.position.z);
                cameraMain.transform.position = new Vector3(transform.position.x, cameraMain.transform.position.y, cameraMain.transform.position.z);
                StartCoroutine(CouldownTP());
            }

        }
        if (collision.gameObject == RightWall)
        {
            if (canTeleport)
            {
                transform.position = new Vector3(leftWall.transform.position.x, transform.position.y, transform.position.z);
                cameraMain.transform.position = new Vector3(transform.position.x, cameraMain.transform.position.y, cameraMain.transform.position.z);
                StartCoroutine(CouldownTP());
            }

        }
    }
    private void LoseLife()
    {
        Debug.Log(lifes);
        Debug.Log("Perde vida!");
        isDead = true;
         lifes--;
        if (lifes <= 0) // Player dies and it's gameover
        {
            GameOver();
        }
        else
        {
            spriteRenderer.enabled = false; // Disable Player          
            bc.enabled = false;
            rb.isKinematic = true;
            loseLifeScreen.gameObject.SetActive(true);
            lifesLeftText.text = lifes.ToString() + "X";
            StartCoroutine(ReloadScene());

        }
    }
    private void GameOver()
    {
        GameManager.gminstance.DisableMainGameLoop();
        GameManager.gminstance.VictoryLoseScreen(gameOver);
        GameManager.gminstance.isCompleted = true;
        audioSource.PlayOneShot(gameOver);
        gameObject.SetActive(false); // Disable Player                                          
        gameOverScreen.gameObject.SetActive(true);
        Cursor.visible = true;
        ScreenTimer.instance.EndTimer(); // Stop counting
        
    }
    private IEnumerator ReloadScene()
    {
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(4f);
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    private IEnumerator CouldownTP()
    {
        canTeleport = false;
        yield return new WaitForSeconds(tpCouldownDuration);
        canTeleport = true;

    }
    
    
}
