﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketExplosion : MonoBehaviour
{
    [SerializeField]
    private GameObject rocket;
    [SerializeField]
    private GameObject explosionRadius;
    [SerializeField]
    private Transform rocketPosition;
    private PlayerHealth player;
    private void Awake()
    {
        player = FindObjectOfType<PlayerHealth>();
        Physics2D.IgnoreCollision(player.GetComponent<BoxCollider2D>(), GetComponent<CircleCollider2D>());
    }

    // Start is called before the first frame update
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag != "Player")
        {
            Destroy(rocket);
            GameObject newRadius = Instantiate(explosionRadius, rocketPosition.position, rocketPosition.rotation);
        }
    }

    private void Update()
    {
        Destroy(rocket, 5);
    }




}
