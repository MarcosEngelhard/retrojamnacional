﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RocketLauncher : MonoBehaviour
{
    [SerializeField]
    private GameObject rocket;
    [SerializeField]
    private float launchForce;
    [SerializeField]
    private Transform shotPoint;  
    private bool shoot = false;
    [SerializeField]
    private Rigidbody2D rb;
    [SerializeField]
    private float knockback = 5;
    [SerializeField]
    private int rocketsLeft = 5;
    [SerializeField]
    private int jamFix = 4;
    private int fixAttempt = 0;
    private Vector2 launchDirection;
    [SerializeField]
    private Animator playerSprite;
    [SerializeField]
    //private GameObject qteObject;
    private QTERocket qteObject;
    [SerializeField]
    private Image bigButtonDisable;
    [SerializeField]
    private Image smallButtonDisable;
    private bool isSlowMotion = false;
    private delegate void LerpToNormalTimeScale();
    private LerpToNormalTimeScale timeScale;
    private AudioSource audioSource;
    [SerializeField]private AudioClip[] explosionAudioClips = new  AudioClip[3];
    [SerializeField] private float maxSpeed = 40f;
    private void Awake()
    {
        qteObject = GetComponent<QTERocket>();
        audioSource = GetComponent<AudioSource>();
        rocketsLeft = 5;
    }
    // Cria um vetor para lançar a bola e deteta input
    void Update()
    {
        if (!Cutscene.cutscene.isOnCutscene && !GameManager.gminstance.isCompleted) // if is not completed or either on cutscene
        {
            if(rb.velocity.magnitude > maxSpeed)
            {
                rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
            }
            launchDirection.x = Input.GetAxisRaw("Horizontal");
            launchDirection.y = Input.GetAxisRaw("Vertical");
            playerSprite.SetFloat("Horizontal", launchDirection.x);
            playerSprite.SetFloat("Vertical", launchDirection.y);
            if (Input.GetButtonDown("Jump") && rocketsLeft > 0)
            {
                shoot = true;
            }
            else if (rocketsLeft < 1) // Start QTE
            {
                if (!isSlowMotion)
                {
                    TimeManager.instance.DoSlowmotion(); // Do the slow motion
                    isSlowMotion = true;
                }

                qteObject.enabled = true; //enable qte component
                if (Input.GetKeyDown(KeyCode.E))
                {

                    fixAttempt++;
                    if (fixAttempt >= jamFix) // Acaba o QTE
                    {

                        rocketsLeft = Random.Range(5, 12);
                        jamFix = Random.Range(3, 7);
                        fixAttempt = 0;
                        qteObject.enabled = false;
                        bigButtonDisable.enabled = false;
                        smallButtonDisable.enabled = false;
                        timeScale = TimeManager.instance.BackToNormal;
                        isSlowMotion = false;
                        //TimeManager.instance.BackToNormal();


                    }
                }
            }
            if (Time.timeScale == 1)
            {
                timeScale = null;
            }
            timeScale?.Invoke();
        }
        else
        {
            if (qteObject.enabled) // Disable
            {
                qteObject.DisableEverything();
                qteObject.enabled = false;
            }
            
        }
        

    }
    // fisica da bola
    private void FixedUpdate()
    {
        if (shoot)
        {
            shoot = false;
            Launch();
        }

    }
    //cria um objeto novo prefab que é a bola
    void Launch()
    {
        AudioClip chosenClip = explosionAudioClips[Random.Range(0, explosionAudioClips.Length)];
        audioSource.PlayOneShot(chosenClip);
        transform.right = launchDirection;
        GameObject newBall = Instantiate(rocket, shotPoint.position, shotPoint.rotation);
        newBall.GetComponent<Rigidbody2D>().AddForce(transform.right * launchForce, ForceMode2D.Impulse);
        rb.AddForce(transform.right * -knockback, ForceMode2D.Impulse);
        rocketsLeft--;

    }
}