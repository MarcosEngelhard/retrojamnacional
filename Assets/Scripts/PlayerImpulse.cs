﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerImpulse : MonoBehaviour
{
    [SerializeField]
    private Rigidbody2D rb;
    [SerializeField]
    private float leapForce = -50f;
    [SerializeField]
    private Transform LeapPosition;

    public void PlayerLeap()
    {
        Vector2 launchPosition = transform.position;
        Vector2 inputPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 launchDirection = inputPosition - launchPosition;
        transform.right = launchDirection;
        
        rb.AddForce(transform.right * leapForce, ForceMode2D.Impulse);
    }
}
