﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cutscene : MonoBehaviour
{
    public static Cutscene cutscene;
    public bool isOnCutscene = false;
    [SerializeField] private GameObject cutsceneImage;
    private DialogueTrigger dialogueTrigger;
    private PlayerHealth player;
    [SerializeField]private Button skipButton;
    private void Awake()
    {
        player = FindObjectOfType<PlayerHealth>();
    }
    // Start is called before the first frame update
    void Start()
    {
        cutscene = this;
        isOnCutscene = true;
        dialogueTrigger = FindObjectOfType<DialogueTrigger>();
        //Change cursor here
        if(player.Lifes == 3)
        {
            DisplayCutscene();
        }
        else
        {
            ToGame();
        }
        
    }
    private void OnEnable()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DisplayCutscene()
    {
        Cursor.visible = true;
        cutsceneImage.SetActive(true);
        skipButton.gameObject.SetActive(true);
        dialogueTrigger.TriggerDialogue();
    }
    public void ToGame()
    {
        Cursor.visible = false;
        cutsceneImage.SetActive(false);
        skipButton.gameObject.SetActive(false);
        isOnCutscene = false;
    }
}
