﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QTERocket : MonoBehaviour
{
    [SerializeField]
    private Image bigButton;
    [SerializeField]
    private Image smallButton;
    [SerializeField]
    private float timeTreshhold = 0;
    private bool bigButtonEnabled = false;
    
    
    void Update()
    {
        //timeTreshhold += Time.deltaTime;
        // Cause we're messing with time scale, unscaleddeltatime should be better
        timeTreshhold += Time.unscaledDeltaTime;
        if(bigButtonEnabled && timeTreshhold > 0.1)
        {
            bigButtonEnabled = false; //bool
            bigButton.enabled = false;
            smallButton.enabled = true;
            timeTreshhold = 0;

        }
        else if(!bigButtonEnabled && timeTreshhold > 0.1)
        {
            bigButtonEnabled = true; //bool
            bigButton.enabled = true;
            smallButton.enabled = false;
            timeTreshhold = 0;
        }
        

    }
    public void DisableEverything()
    {
        bigButton.enabled = false;
        smallButton.enabled = false;
        timeTreshhold = 0;
    }

    
}
