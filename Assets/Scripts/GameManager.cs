﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager gminstance;
    [SerializeField] private Image getChopper;
    [SerializeField] private Image destroyChopper;
    [SerializeField] private Image defeatEnemy;
    [SerializeField] private GameObject CutsceneSound;
    [SerializeField] private GameObject MainGameLoopSound;
    private AudioSource audioSource;
    [SerializeField]private AudioClip victory;
    [SerializeField] private AudioClip anthem;
    [SerializeField] private AudioClip gameOver;
    private PlayerHealth player;
    public bool isCompleted = false;
    
    
    private void Awake()
    {
        gminstance = this;
        player = FindObjectOfType<PlayerHealth>();
        audioSource = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        getChopper.gameObject.SetActive(false);
        destroyChopper.gameObject.SetActive(false);
        defeatEnemy.gameObject.SetActive(false);
        if(player.Lifes == 3)
        {
            CutsceneSound.SetActive(true);
            MainGameLoopSound.SetActive(false);
        }
        else
        {
            CutsceneSound.SetActive(false);
            MainGameLoopSound.SetActive(true);
        }       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DisplayGetChopperImage() // Show get chopper screen
    {
        getChopper.gameObject.SetActive(true);
        ScreenTimer.instance.EndTimer();
        Time.timeScale = 0f;
        isCompleted = true;
        Cursor.visible = true;
        VictoryLoseScreen(victory);

    }
    public void DisplayDestroyChopperImage() // show destroy chopper cutscene
    {
        destroyChopper.gameObject.SetActive(true);
        ScreenTimer.instance.EndTimer();
        Time.timeScale = 0f;
        isCompleted = true;
        Cursor.visible = true;
        VictoryLoseScreen(gameOver);
    }
    public void DisplayDestroyEnemyImage() // show destroy enemy cutscene
    {
        defeatEnemy.gameObject.SetActive(true);
        ScreenTimer.instance.EndTimer();
        Time.timeScale = 0f;
        isCompleted = true;
        Cursor.visible = true;
        VictoryLoseScreen(anthem);
    }
    public void ChangeClipForMainAudioSource()
    {
        ScreenTimer.instance.BeginTimer();
        CutsceneSound.SetActive(false);
        MainGameLoopSound.SetActive(true);
             
    }
    public void VictoryLoseScreen(AudioClip clipfortheEnding)
    {
        DisableMainGameLoop();
        //audioSource.PlayOneShot(victory);
        audioSource.clip = clipfortheEnding;
        audioSource.Play();
        audioSource.loop = true;
    }
    public void DisableMainGameLoop()
    {
        MainGameLoopSound.SetActive(false);
    }
}
