﻿
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private float playerDistanceSpawnLevelPart = 100f;
    [SerializeField] private Transform levelPartStart;
    [SerializeField] private List<Transform> levels;
    private Vector3 lastEndPosition;
    [SerializeField] private Transform player;
    [SerializeField] private int startingSpawnLevel = 2;
    [SerializeField] private float distanceToNowSpawn = 300f;
    [SerializeField] private Transform finalPart;
    private bool isFinalPartDeployed = false;
    private void Awake()
    {
        lastEndPosition = levelPartStart.Find("EndPosition").position;
        for(int i = 0; i < startingSpawnLevel; i++)
        {
            SpawnLevelPart();
        }
    }
    private void Update()
    {
        if(lastEndPosition.y  < distanceToNowSpawn)
        {
            if (Vector2.Distance(player.transform.position, lastEndPosition) < playerDistanceSpawnLevelPart)
            {
                // Spawn Another Level Part
                SpawnLevelPart();
            }
        }
        else
        {
            if (isFinalPartDeployed)
            {
                return; // exit function 'cause Final part is already deployed
            }
            Instantiate(finalPart, lastEndPosition, Quaternion.identity);
            isFinalPartDeployed = true;
        }
        
    }
    private void SpawnLevelPart()
    { // Spawn a level part which is chosen by random
        Transform chosenLevelPart = levels[Random.Range(0, levels.Count)];
        Transform lastLevelPartTransform = SpawnLevelPart(chosenLevelPart,lastEndPosition);
        lastEndPosition = lastLevelPartTransform.Find("EndPosition").position;
    }
    private Transform SpawnLevelPart(Transform levelPart, Vector3 spawnPosition)
    {
        Transform levelPartTransform = Instantiate(levelPart, spawnPosition, Quaternion.identity);
        return levelPartTransform;
    }
}
