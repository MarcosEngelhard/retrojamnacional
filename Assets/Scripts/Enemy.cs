﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]private Transform attackFirePoint;
    [SerializeField] private float speed;
    private delegate void Attack();
    private Attack attack;
    [SerializeField]private GameObject laser;
    private SpriteRenderer laseSpriteRenderer;
    private bool isPlayerSpoted = false;
    [SerializeField]private Transform player;
    [SerializeField] private int maxHealth = 200;
    private int currenHealth;
    private float tiledHeight = 0;
    [SerializeField] private float limitTiledHeight = 200f;
    [SerializeField]private float tileChangeSpeed = 0.7f;
    private BoxCollider2D laserBC;
    //private TimeManager timeManager;
    private enum AttackType
    {
        Laser, Spike, FireBall
    }
    [SerializeField] private AttackType attackType;  
    [HideInInspector]public SpikePool spikePool;
    [HideInInspector] public FirePool firePool;
    [SerializeField] private int fireBallsToTrigger = 2;
    [SerializeField] private int spikeToTrigger = 4;
    [SerializeField]private float distanceBetweenMax = 10f;
    private Rigidbody2D rb;
    [SerializeField]private float maxAngle = 165;
    [SerializeField] private float minAngle = 25f;
    private bool isIdle = true;
    [SerializeField] private float changeAttackDuration = 4f; // this is the waiting to change attack
    private bool LaserOnCourotine = false;
    private AudioSource audioSource;
    [SerializeField]private AudioClip prepareLaserClip;
    [SerializeField]private AudioClip[] laserSound = new AudioClip[3];
    [SerializeField] private AudioClip throwProjectile;
    private SpriteRenderer enemySpriteRenderer;
    private Color initialColor;
    // Start is called before the first frame update
    void Start()
    {
        // Setup to change sprite's color
        enemySpriteRenderer = GetComponent<SpriteRenderer>();
        initialColor = enemySpriteRenderer.color;
        spikePool = transform.Find("SpikePool").GetComponent<SpikePool>();
        firePool = transform.Find("FirePool").GetComponent<FirePool>();
        rb = GetComponent<Rigidbody2D>();
        currenHealth = maxHealth;
        //timeManager = FindObjectOfType<TimeManager>();
        laseSpriteRenderer = laser.GetComponent<SpriteRenderer>();
        laser.SetActive(false);
        audioSource = GetComponent<AudioSource>();
        laserBC = laseSpriteRenderer.GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!Cutscene.cutscene.isOnCutscene)
        {
            attack?.Invoke();
            if (isIdle && attack == null) // after the cutscene, make enemy starts to attack
            {
                Invoke("ChooseAttack", 7f);
                isIdle = false;
            }

        }

    }
    private void FixedUpdate()
    {
        if (!Cutscene.cutscene.isOnCutscene)
        {
            MoveEnemy();
        }
    }
    private void MoveEnemy()
    {
        float distnaceInY = Mathf.Abs(player.transform.position.y - transform.position.y);
        if(distnaceInY > distanceBetweenMax)
        {
            // Head up, enemy is moving up when player move away
            rb.velocity = Vector2.up * speed;
        }
    }
    private void PrepareLaser() // Do a sound in order to Warn Player and invoke after the sound stops
    {
        audioSource.PlayOneShot(prepareLaserClip);
        Invoke("LaserReady", 3);
        attack = null;
    }
    private void LaserReady()
    {
        audioSource.Stop();
        attack = LaserBeam;
    }
    public void LaserBeam()
    {
        if (!GameManager.gminstance.isCompleted)
        {
            if (!isPlayerSpoted)
            { // spot enemy and enable line renderer
                laser.SetActive(true);
                isPlayerSpoted = true;

            }
            IncreaseLaser();
        }
        
        
         
    }
    private void IncreaseLaser()
    {
        if (tiledHeight < limitTiledHeight)
        {
            if (!audioSource.isPlaying)
            {
                AudioClip chosenClip = laserSound[Random.Range(0, laserSound.Length)];
                audioSource.PlayOneShot(chosenClip);
            }
            tiledHeight += tileChangeSpeed * Time.fixedDeltaTime;
            laseSpriteRenderer.size = new Vector2(laseSpriteRenderer.size.x, tiledHeight);
            //laserBC.center = new Vector3(laseSpriteRenderer.bounds.center);
            laserBC.size = laseSpriteRenderer.size;
        }
        else
        {
            if (!LaserOnCourotine) // In case it's not in courotine to stop attack
            {
                StartCoroutine(StopAttack());
                LaserOnCourotine = true;
            }

        }
    }
    private void ChooseAttack()
    {
        if (!GameManager.gminstance.isCompleted)
        {
            // Pick randomly the attackType
            attackType = (AttackType)Random.Range(0, System.Enum.GetValues(typeof(AttackType)).Length);
            switch (attackType)
            {
                case AttackType.Laser:
                    attack = PrepareLaser;
                    break;
                case AttackType.Spike:
                    attack = ThrowSpikes;
                    break;
                case AttackType.FireBall:
                    attack = ThrowFireBalls;
                    break;
            }
        }
        
    }

    private void ThrowSpikes()
    {
        if (!GameManager.gminstance.isCompleted)
        {
            attack = null;
            StartCoroutine(ThrowProjectiles());
        }
        
    }
    private void ThrowFireBalls()
    {
        if (!GameManager.gminstance.isCompleted)
        {
            attack = null;
            StartCoroutine(ThrowFireProjectiles());
        }
           
    }
    private IEnumerator ThrowProjectiles()
    {
        for (int index = 0; index < fireBallsToTrigger; index++)
        {
            audioSource.PlayOneShot(throwProjectile);
            RotateFirePoint();
            var projectile = spikePool.Get();
            projectile.transform.parent = null; // detach to parent
            projectile.transform.position = attackFirePoint.position;
            projectile.transform.localRotation = attackFirePoint.localRotation;
            projectile.gameObject.SetActive(true);
            yield return new WaitForSeconds(1f);
        }
        Invoke("ChooseAttack", 2f);
    }
    private void RotateFirePoint()
    {
        float rotZ = Random.Range(minAngle, maxAngle + 1);
        attackFirePoint.rotation = Quaternion.Euler(new Vector3(0, 0, rotZ));
    }
    private IEnumerator ThrowFireProjectiles()
    {       
        for (int index = 0; index < spikeToTrigger; index++)
        {
            RotateFirePoint();
            var projectile = firePool.Get();
            projectile.transform.parent = null; // detach to parent
            projectile.transform.position = attackFirePoint.position;
            projectile.transform.localRotation = attackFirePoint.localRotation;
            projectile.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.5f);
        }
        StartCoroutine(StopAttack());
    }
    private IEnumerator StopAttack()
    {       
        yield return new WaitForSeconds(changeAttackDuration);
        switch (attackType)
        {
            case AttackType.Laser: // disable laser
                laser.gameObject.SetActive(false);
                isPlayerSpoted = false;
                attack = null;
                LaserOnCourotine = false;
                break;
            case AttackType.Spike:
                break;
            case AttackType.FireBall:
                break;
        }
        ChooseAttack();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("PlayerProjectile"))
        {
            TakeDamage(1);
            Destroy(collision.gameObject); // Destroy projectile
        }
    }
    public void TakeDamage(int amount)
    {
        
        // lose health
        currenHealth -= amount;
        StopCoroutine(DamageColor());
        StartCoroutine(DamageColor());
        if(currenHealth <= 0)
        {
            Die();
        }
    }
    private IEnumerator DamageColor()
    { // Change sprite renderer whenver it's hitten
        enemySpriteRenderer.color = Color.red;
        yield return new WaitForSeconds(.5f);
        enemySpriteRenderer.color = initialColor;
    }
    private void Die()
    { // Enemy die!
        GameManager.gminstance.DisplayDestroyEnemyImage();
        gameObject.SetActive(false);
    }
}
