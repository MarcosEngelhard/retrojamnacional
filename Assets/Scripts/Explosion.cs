﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    [SerializeField]
    private Transform explosionPosition;
    [SerializeField]
    private float yeetForce = 30f;
    [SerializeField]
    private GameObject radiusDestroy;
    
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Transform playerPosition = collision.gameObject.GetComponent<Transform>();
            Vector2 explosionVector = explosionPosition.position;
            Vector2 playerVector = playerPosition.position;
            Vector2 playerYeet = playerVector - explosionVector;
            transform.right = playerYeet;
            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right * yeetForce, ForceMode2D.Impulse);
        }
        Destroy(radiusDestroy);


    }

    private void Update()
    {
        Destroy(radiusDestroy, .2f);
    }




}
