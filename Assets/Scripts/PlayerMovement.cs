﻿
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float movementSpeed = 2f;
    [SerializeField]
    private Rigidbody2D rb;
    private float movementInput;

    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float movement = Input.GetAxis("Horizontal");
        movementInput = movement;
        
        
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(movementInput * movementSpeed, rb.velocity.y);
    }
}
